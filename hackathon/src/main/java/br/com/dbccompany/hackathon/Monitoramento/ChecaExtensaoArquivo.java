package br.com.dbccompany.hackathon.hackathon.Monitoramento;

import java.nio.file.Path;
import java.util.Optional;

public class ChecaExtensaoArquivo {

    private Optional<String> getExtensaoDoArquivo(String filename) {
        return Optional.ofNullable(filename)
                .filter(f -> f.contains("."))
                .map(f -> f.substring(filename.lastIndexOf(".") + 1));
    }

    public boolean isArquivoDat(Path arquivo) {
        Optional<String> extensao = getExtensaoDoArquivo(String.valueOf(arquivo));
        return extensao.isPresent() && extensao.get().equals("dat");
    }
}
