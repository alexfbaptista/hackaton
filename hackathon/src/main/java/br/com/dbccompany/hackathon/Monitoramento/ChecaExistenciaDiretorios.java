package br.com.dbccompany.hackathon.hackathon.Monitoramento;

import br.com.dbccompany.hackathon.hackathon.URI.URI;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;

@Component
public class ChecaExistenciaDiretorios {

    @PostConstruct
    public static void output() {
        if (!Files.exists(Path.of(URI.OUTPUT))) {
            File file = new File(Path.of(URI.OUTPUT).toString());
            file.mkdirs();
        }
    }

    @PostConstruct
    private void input() {
        if (!Files.exists(Path.of(URI.INPUT))) {
            File file = new File(Path.of(URI.INPUT).toString());
            file.mkdirs();
        }
    }

}
